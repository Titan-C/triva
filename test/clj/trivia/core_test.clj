(ns trivia.core-test
  (:require [trivia.core :as sut]
            [clojure.test :as t]))

(t/deftest testing-add
  (t/is (= (sut/adds 3 4) 7)))
