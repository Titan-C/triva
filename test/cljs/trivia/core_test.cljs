(ns trivia.core-test
  (:require [trivia.core :as sut]
            [cljs.test :as t :include-macros true]))

(t/deftest testing-clojuescript
  (t/is (= (sut/adds 3 1) 4)))
