(ns trivia.runner
  (:require  [cljs.test :as t :include-macros true]
             [doo.runner :refer-macros [doo-tests]]
             [trivia.core-test]))

(doo-tests 'trivia.core-test)
