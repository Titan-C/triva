(ns trivia.core
  (:require [reagent.core :as r]))

(defn adds [a b] (+ a b))

(defn mount-root []
  (r/render [:h1 "hello tu"]
                  (.getElementById js/document "app")))

(defn ^:export init []
  (mount-root))
